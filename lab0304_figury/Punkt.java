package com.company;

import java.awt.geom.Point2D;

public class Punkt extends Point2D{
    private double x;
    private double y;

    Punkt(){
        x = 0;
        y = 0;
    }

    Punkt(double x, double y){
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getY() {
        return y;
    }

    @Override
    public void setLocation(double x, double y) {

    }

    public String toString() {
        return "("+String.valueOf(x)+","+String.valueOf(y)+")";
    }
}
