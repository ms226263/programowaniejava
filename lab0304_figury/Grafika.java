package com.company;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Random;

public class Grafika extends JPanel implements KeyListener, MouseListener {

    protected LinkedList<Figura> listaFigur;
    int nrFigury = -1;





    Grafika(LinkedList<Figura> listaFigur){

        this.listaFigur = listaFigur;
        setFocusable(true);
        addKeyListener(this);
        addMouseListener(this);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setStroke(new BasicStroke(3.0f));
        g2d.drawString("A - Dodaj losową figurę; F - wczytaj z pliku",20,500);



        for(int i = 0; i< listaFigur.size();i++){
            //Figura fig = listaFigur.get(i);
            if(listaFigur.get(i).getClass()==Okrag.class)
            {
                if(listaFigur.get(i).isSelected()) g2d.setColor(Color.BLUE);
                else g2d.setColor(Color.GREEN);
                Okrag okr = (Okrag) listaFigur.get(i);
                g2d.draw(okr.getShape());
            }
            if(listaFigur.get(i).getClass()==Trojkat.class)
            {
                if(listaFigur.get(i).isSelected()) g2d.setColor(Color.BLUE);
                else g2d.setColor(Color.RED);
                Trojkat trj = (Trojkat) listaFigur.get(i);
                g2d.draw(trj.getShape());
            }
            if(listaFigur.get(i).getClass()==Kwadrat.class)
            {
                if(listaFigur.get(i).isSelected()) g2d.setColor(Color.BLUE);
                else g2d.setColor(Color.ORANGE);
                Kwadrat kwd = (Kwadrat) listaFigur.get(i);
                g2d.draw(kwd.getShape());
            }
        }




    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {


        //Wczytywanie figur z pliku////////////////////////////////////////////////////
        if(e.getKeyCode() == KeyEvent.VK_F)
        {
            try {
                listaFigur = Figura.getFromFile("figury.txt");
                this.repaint();
            } catch (FileNotFoundException ex) {
                System.err.println("Error opening ");
            }

        }
        /////////////////////////////////////////////////////////////////////////////////
        //Dodanie losowej figury////////////////////////////////////////////////////
        if(e.getKeyCode() == KeyEvent.VK_A)
        {
            Random rand = new Random();
            int temp = rand.nextInt()%3;
            if(temp == 0) listaFigur.add(new Kwadrat());
            if(temp == 1) listaFigur.add(new Okrag());
            if(temp == 2) listaFigur.add(new Trojkat());
            this.repaint();
        }
        /////////////////////////////////////////////////////////////////////////////////




        double skok = 5;
        if(nrFigury >=0) {
            Figura selected = listaFigur.get(nrFigury);
            //Przesuwanie okregu/////////////////////////////////////////////
            if (selected.getClass() == Okrag.class) {
                Okrag okrag = (Okrag) listaFigur.get(nrFigury);
                if (e.getKeyCode() == KeyEvent.VK_RIGHT) {

                    double temp = okrag.getSrodek().getX();
                    okrag.getSrodek().setX(temp + skok);
                    listaFigur.set(nrFigury, okrag);
                    this.repaint();
                }
                if (e.getKeyCode() == KeyEvent.VK_LEFT) {

                    double temp = okrag.getSrodek().getX();
                    okrag.getSrodek().setX(temp - skok);
                    listaFigur.set(nrFigury, okrag);
                    this.repaint();
                }
                if (e.getKeyCode() == KeyEvent.VK_UP) {

                    double temp = okrag.getSrodek().getY();
                    okrag.getSrodek().setY(temp - skok);
                    listaFigur.set(nrFigury, okrag);
                    this.repaint();
                }
                if (e.getKeyCode() == KeyEvent.VK_DOWN) {

                    double temp = okrag.getSrodek().getY();
                    okrag.getSrodek().setY(temp + skok);
                    listaFigur.set(nrFigury, okrag);
                    this.repaint();
                }

            }
            /////////////////////////////////////////////////////////////////////////////////
            //Przesuwanie kwadratu///////////////////////////////////////////////////////////
            if (selected.getClass() == Kwadrat.class) {
                Kwadrat kwadrat = (Kwadrat) listaFigur.get(nrFigury);
                if (e.getKeyCode() == KeyEvent.VK_RIGHT) {

                    double temp = kwadrat.getLewyGorny().getX();
                    kwadrat.getLewyGorny().setX(temp + skok);
                    listaFigur.set(nrFigury, kwadrat);
                    this.repaint();
                }
                if (e.getKeyCode() == KeyEvent.VK_LEFT) {

                    double temp = kwadrat.getLewyGorny().getX();
                    kwadrat.getLewyGorny().setX(temp - skok);
                    listaFigur.set(nrFigury, kwadrat);
                    this.repaint();
                }
                if (e.getKeyCode() == KeyEvent.VK_UP) {

                    double temp = kwadrat.getLewyGorny().getY();
                    kwadrat.getLewyGorny().setY(temp - skok);
                    listaFigur.set(nrFigury, kwadrat);
                    this.repaint();
                }
                if (e.getKeyCode() == KeyEvent.VK_DOWN) {

                    double temp = kwadrat.getLewyGorny().getY();
                    kwadrat.getLewyGorny().setY(temp + skok);
                    listaFigur.set(nrFigury, kwadrat);
                    this.repaint();
                }

            }
            /////////////////////////////////////////////////////////////////////////////////
            //Przesuwanie trojkata///////////////////////////////////////////////////////////
            if (selected.getClass() == Trojkat.class) {
                Trojkat trojkat = (Trojkat) listaFigur.get(nrFigury);
                if (e.getKeyCode() == KeyEvent.VK_RIGHT) {

                    double temp1 = trojkat.getA().getX();
                    double temp2 = trojkat.getB().getX();
                    double temp3 = trojkat.getC().getX();
                    trojkat.getA().setX(temp1 + skok);
                    trojkat.getB().setX(temp2 + skok);
                    trojkat.getC().setX(temp3 + skok);
                    listaFigur.set(nrFigury, trojkat);
                    this.repaint();
                }
                if (e.getKeyCode() == KeyEvent.VK_LEFT) {

                    double temp1 = trojkat.getA().getX();
                    double temp2 = trojkat.getB().getX();
                    double temp3 = trojkat.getC().getX();
                    trojkat.getA().setX(temp1 - skok);
                    trojkat.getB().setX(temp2 - skok);
                    trojkat.getC().setX(temp3 - skok);
                    listaFigur.set(nrFigury, trojkat);
                    this.repaint();
                }
                if (e.getKeyCode() == KeyEvent.VK_UP) {

                    double temp1 = trojkat.getA().getY();
                    double temp2 = trojkat.getB().getY();
                    double temp3 = trojkat.getC().getY();
                    trojkat.getA().setY(temp1 - skok);
                    trojkat.getB().setY(temp2 - skok);
                    trojkat.getC().setY(temp3 - skok);
                    listaFigur.set(nrFigury, trojkat);
                    this.repaint();
                }
                if (e.getKeyCode() == KeyEvent.VK_DOWN) {

                    double temp1 = trojkat.getA().getY();
                    double temp2 = trojkat.getB().getY();
                    double temp3 = trojkat.getC().getY();
                    trojkat.getA().setY(temp1 + skok);
                    trojkat.getB().setY(temp2 + skok);
                    trojkat.getC().setY(temp3 + skok);
                    this.repaint();

                }

            }
            /////////////////////////////////////////////////////////////////////////////////
            //Usuwanie zaznaczonej figury////////////////////////////////////////////////////
            if(e.getKeyCode() == KeyEvent.VK_DELETE)
            {
                System.out.println(listaFigur.size());
                listaFigur.remove(nrFigury);
                this.repaint();
                System.out.println(listaFigur.size());
                nrFigury = -1;
            }
            /////////////////////////////////////////////////////////////////////////////////


        }

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void mouseClicked(MouseEvent e) {
        System.out.println("Coś wcisnieto");
        if(SwingUtilities.isRightMouseButton(e)){
            boolean temp = false;
            Punkt punkt = new Punkt(e.getX(),e.getY());
            for(int i=0;i<listaFigur.size();i++)
            {
                listaFigur.get(i).setSelected(false);
                if(listaFigur.get(i).isInside(punkt))
                {
                    nrFigury = i;
                    temp = true;
                }

            }

            if(temp == false)   nrFigury = -1;
            else    listaFigur.get(nrFigury).setSelected(true);

            this.repaint();

            System.out.println("Wybrano: " + nrFigury);
            System.out.println("Kliknieto w : " + punkt);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
