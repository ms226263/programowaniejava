package com.company;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import javax.swing.*;
import java.util.LinkedList;

public class Main extends JFrame {



    public static void main(String args[]) throws FileNotFoundException {

        LinkedList<Figura> figury = new LinkedList<>();
        //LinkedList<Figura> figury = Figura.getFromFile("figury.txt");
        //LinkedList<Figura> figury = Figura.getRandom(5);

        JFrame okno = new JFrame();
        Grafika grafika = new Grafika(figury);
        //USTAWIENIA OKNA
        okno.setSize(600,600);
        okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        okno.setTitle("Figury");
        okno.setLocationRelativeTo(null);
        okno.setBackground(Color.BLACK);
        okno.add(grafika);
        okno.repaint();
        okno.setVisible(true);




    }
}