package com.company;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.util.Random;

public class Okrag extends Figura {

    private Punkt srodek;
    private int promien;

    Okrag(){
        Random rand = new Random();
        this.srodek = new Punkt(Math.abs(rand.nextInt())%300,Math.abs(rand.nextInt())%300);
        this.promien = 1 + Math.abs(rand.nextInt())%100;
    }

    Okrag(Punkt Srodek, int Promien){
        this.srodek = Srodek;
        this.promien = Promien;
    }

    public void setPromien(int promien) {
        promien = promien;
    }

    public void setSrodek(Punkt srodek) {
        this.srodek = srodek;
    }

    public int getPromien() {
        return promien;
    }

    public Punkt getSrodek() {
        return srodek;
    }

    public String toString() {
        return "Promien: " + String.valueOf(promien) + " srodek: " + srodek.toString();
    }

    public Shape getShape(){
        return new Ellipse2D.Double(srodek.getX()-promien,srodek.getY()-promien,2*promien,2*promien);
    }

    public boolean isInside(Punkt punkt){
        //if((punkt.getX()-srodek.getX())*(punkt.getX()-srodek.getX())+(punkt.getY()-srodek.getY())*(punkt.getY()-srodek.getY()) <= promien*promien) return true;
        //else return false;
        return getShape().contains(punkt);
    }




}

