package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Random;
import java.util.Scanner;

public class Figura {

    private boolean selected;



    Figura() { selected = false ;}

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isSelected() {
        return selected;
    }

    public static LinkedList<Figura> getFromFile(String filename) throws FileNotFoundException{

        LinkedList<Figura> listaFigur = new LinkedList<>();

        File file = new File(filename);
        Scanner in = new Scanner(file);
        while(in.hasNextLine()){
            String linia = in.nextLine();
            String temp[] = linia.split(" ");


            if(temp[0].equals("k")) listaFigur.add(new Kwadrat(new Punkt(Integer.parseInt(temp[1]),Integer.parseInt(temp[2])), Integer.parseInt(temp[3])));
            if(temp[0].equals("o")) listaFigur.add(new Okrag(new Punkt(Integer.parseInt(temp[1]),Integer.parseInt(temp[2])),Integer.parseInt(temp[3])));
            if(temp[0].equals("t")) listaFigur.add(new Trojkat(new Punkt(Integer.parseInt(temp[1]),Integer.parseInt(temp[2])),
                                                            new Punkt(Integer.parseInt(temp[3]),Integer.parseInt(temp[4])),
                                                             new Punkt(Integer.parseInt(temp[5]),Integer.parseInt(temp[6]))));


        }

        System.out.println("Liczba wczytanych figur: " + listaFigur.size());
        return listaFigur;
    }

    public static LinkedList<Figura> getRandom(int liczba){

        LinkedList<Figura> listaFigur = new LinkedList<>();

        Random rand = new Random();
        int temp = rand.nextInt();


        for(int i = 0;i<liczba;i++)
        {
            temp = Math.abs(rand.nextInt());
            temp %= 3;
            System.out.println(temp);
            if(temp == 0) listaFigur.add(new Kwadrat());
            if(temp == 1) listaFigur.add(new Okrag());
            if(temp == 2) listaFigur.add(new Trojkat());
        }

        System.out.println("Liczba wylosowanych figur: " + listaFigur.size());
        return listaFigur;
    }

    public boolean isInside(Punkt punkt){
        return false;
    }

}
