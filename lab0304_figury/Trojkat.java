package com.company;

import java.awt.*;
import java.awt.geom.GeneralPath;
import java.awt.geom.Point2D;
import java.util.Random;

public class Trojkat extends Figura{

    private Punkt a;
    private Punkt b;
    private Punkt c;

    Trojkat(){
        Random rand = new Random();
        a = new Punkt(Math.abs(rand.nextInt())%300,Math.abs(rand.nextInt())%300);
        b = new Punkt(Math.abs(rand.nextInt())%300,Math.abs(rand.nextInt())%300);
        c = new Punkt(Math.abs(rand.nextInt())%300,Math.abs(rand.nextInt())%300);

    }

    Trojkat(Punkt a, Punkt b, Punkt c){
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public Punkt getA() {
        return a;
    }

    public Punkt getB() {
        return b;
    }

    public Punkt getC() {
        return c;
    }

    public void setA(Punkt a) {
        this.a = a;
    }

    public void setB(Punkt b) {
        this.b = b;
    }

    public void setC(Punkt c) {
        this.c = c;
    }


    public String toString() {
        return "Wierzcholek 1:" + a.toString() +"  Wierzcholek 2:" + b.toString() +"  Wierzcholek 3:" + c.toString();
    }

    public Shape getShape(){
        GeneralPath trojkat = new GeneralPath();
        trojkat.moveTo(c.getX(), c.getY());
        trojkat.lineTo(a.getX(), a.getY());
        trojkat.lineTo(b.getX(), b.getY());
        trojkat.lineTo(c.getX(), c.getY());
        trojkat.closePath();
        return trojkat;
    }

    public boolean isInside(Punkt punkt){
        return getShape().contains(punkt);
    }


}
