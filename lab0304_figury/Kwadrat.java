package com.company;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.util.Random;

public class Kwadrat extends Figura{

    private Punkt lewyGorny;
    private int bok;

    Kwadrat(){
        Random rand = new Random();
        this.lewyGorny = new Punkt(Math.abs(rand.nextInt())%300,Math.abs(rand.nextInt())%300);
        this.bok = 1 + Math.abs(rand.nextInt())%100;
    }

    Kwadrat(Punkt LewyGorny, int bok){
        this.lewyGorny = LewyGorny;
        this.bok = bok;
    }

    public Punkt getLewyGorny() {
        return lewyGorny;
    }

    public void setLewyGorny(Punkt lewyGorny) {
        lewyGorny = lewyGorny;
    }

    public int getBok() {
        return bok;
    }

    public void setBok(int bok) {
        this.bok = bok;
    }

    public String toString() {
        return "Lewy gorny rog: "+lewyGorny.toString()+" dlugosc boku: "+bok;
    }

    public Shape getShape(){
        return new Rectangle2D.Double(lewyGorny.getX(),lewyGorny.getY(),bok,bok);
    }

    public boolean isInside(Punkt punkt){
        //if( (punkt.getX() >= lewyGorny.getX() && punkt.getX() <= (lewyGorny.getX()+bok)) && (punkt.getY() >= lewyGorny.getY() && punkt.getY() <= (lewyGorny.getY()+bok))) return true;
        //else return false;
        return getShape().contains(punkt);

    }
}

