package com.company;

public class Punkt_1D {

    protected double x;
    protected double id;

    public double getX() {
        return x;
    }

    public double getId() {
        return id;
    }

    public void setX(double x){
        this.x = x;
    }

    public void setId(double id){
        this.id = id;
    }

    Punkt_1D(double x)
    {
        this.x = x;
    }

    Punkt_1D()
    {
        this.x = 0;
    }

    public String toString() {
        return "(" + Double.toString(x) + ")";
    }

    public double ileOd0() {
        return x;
    }
}
