package com.company;

public class Punkt_3D extends Punkt_2D{

    protected double z;

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    Punkt_3D(double x, double y, double z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    Punkt_3D(){

    }

    public String toString() {
        return "(" + Double.toString(x) + "," + Double.toString(y) + ","  + Double.toString(z) + ")";
    }

    public double ileOd0() {
        return Math.sqrt(x*x + y*y + z*z);
    }
}
