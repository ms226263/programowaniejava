package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {

        File file = new File("punkty.txt");
        Scanner in = new Scanner(file);
        List<Punkt_1D> listaPunktow = new LinkedList<>();

        while(in.hasNextLine()){
            String linia = in.nextLine();
            String temp[] = linia.split(" ");
            if(temp.length == 3){
                Punkt_3D pkt = new Punkt_3D(Double.parseDouble(temp[0]),Double.parseDouble(temp[1]),Double.parseDouble(temp[2]));
                listaPunktow.add(pkt);
            }
            else if(temp.length == 2) {
                Punkt_2D pkt = new Punkt_2D(Double.parseDouble(temp[0]),Double.parseDouble(temp[1]));
                listaPunktow.add(pkt);
            }
            else if(temp.length == 1) {
                Punkt_1D pkt = new Punkt_1D(Double.parseDouble(temp[0]));
                listaPunktow.add(pkt);
            }
        }

        System.out.println("\n---Wczytane punkty:---");
        for(int i = 0 ; i < listaPunktow.size() ; i++) {
            System.out.println(listaPunktow.get(i));
            System.out.println("Odległość od punktu 0: " + listaPunktow.get(i).ileOd0() + "\n");
        }

        Collections.sort(listaPunktow, new Comparator<Punkt_1D>() {
            @Override
            public int compare(Punkt_1D c1, Punkt_1D c2) {
                return Double.compare(c1.ileOd0(), c2.ileOd0());
            }
        });

        System.out.println("\n---Posortowana lista---\n");

        for(int i = 0 ; i < listaPunktow.size() ; i++) {
            System.out.println(listaPunktow.get(i));
            System.out.println("Odległość od punktu 0: " + listaPunktow.get(i).ileOd0() + "\n");
        }


    }
}
