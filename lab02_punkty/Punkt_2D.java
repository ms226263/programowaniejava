package com.company;

public class Punkt_2D extends Punkt_1D{

    protected double y;

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    Punkt_2D(double x, double y)
    {
        this.x = x;
        this.y = y;
    }

    Punkt_2D(){

    }

    public String toString() {
        return "(" + Double.toString(x) + "," + Double.toString(y) + ")";
    }

    public double ileOd0() {
        return Math.sqrt(x*x + y*y);
    }
}
