import java.util.Random;

public class Zespolona {

    private double a;
    private double b;

    public double getA(){
        return a;
    }

    public double getB(){
        return b;
    }

    public void setA(double a){
        this.a = a;
    }

    public void setB(double b){
        this.b = b;
    }

    Zespolona(double a, double b){
        this.a = a;
        this.b = b;
    }

    Zespolona(){
        Random rand = new Random();
        this.a = rand.nextDouble()*22;
        this.b = rand.nextDouble()*27;
    }

    public void wyswietlanie(){

        if(b>0 && a != 0) System.out.print(a+" +"+b+"i");
        else if (b == 0) System.out.print(a);
        else if (a == 0 && b!=0) System.out.print(b+"i");
        else System.out.print(a+" "+b+"i");

    }

    public Zespolona sprzezenie(){
        Zespolona temp = new Zespolona(a,-b);
        return temp;
    }

    public double modul(){
        return Math.sqrt(a*a + b*b);
    }

    public Zespolona dodawanie(Zespolona z){
        Zespolona temp = new Zespolona(this.a + z.getA() , this.b + z.getB());
        return temp;
    }

    public Zespolona odejmowanie(Zespolona z){
        Zespolona temp = new Zespolona(this.a - z.getA() , this.b - z.getB());
        return temp;
    }

    public Zespolona mnozenie(Zespolona z){
        Zespolona temp = new Zespolona(this.a*z.getA() - this.b*z.getB(), this.a*z.getB() + this.b*z.getA());
        return temp;
    }




}
