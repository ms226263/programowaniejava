import java.text.DecimalFormat;
import java.util.Scanner;

class Glowna {
    public static void main(String[] args) {
        DecimalFormat df = new DecimalFormat("#.00");

        Scanner odczyt = new Scanner(System.in);

        double a,b;
        Zespolona[] tab = new Zespolona[5];

        System.out.println("Podaj wartość części rzeczywistej: ");
        a = odczyt.nextDouble();
        System.out.println("Podaj wartość części urojonej (bez i): ");
        b = odczyt.nextDouble();

        Zespolona liczbaUzytkownika = new Zespolona(a,b);

        System.out.println("Podana liczba to: ");
        liczbaUzytkownika.wyswietlanie();

        System.out.println("\nTablica 5 losowych liczb zespolonych: ");
        for(int i = 0; i<5; i++){
            tab[i] = new Zespolona();
            tab[i].wyswietlanie();
            System.out.println("\n");
        }
        System.out.println("\nSprzężenie liczby podanej przez użytkownika: ");
        liczbaUzytkownika.sprzezenie().wyswietlanie();
        System.out.println("\nModul liczby podanej przez użytkownika: ");
        System.out.print(liczbaUzytkownika.modul());
        System.out.println("\nSuma liczby podanej przez użytkownika i jej sprzezenia: ");
        liczbaUzytkownika.dodawanie(liczbaUzytkownika.sprzezenie()).wyswietlanie();
        System.out.println("\nRoznica liczby podanej przez użytkownika i jej sprzezenia: ");
        liczbaUzytkownika.odejmowanie(liczbaUzytkownika.sprzezenie()).wyswietlanie();
        System.out.println("\nIloczyn liczby podanej przez użytkownika i jej sprzezenia: ");
        liczbaUzytkownika.mnozenie(liczbaUzytkownika.sprzezenie()).wyswietlanie();


    }


}

